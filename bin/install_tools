#!/usr/bin/env bash
set -exuo pipefail

# Intended for use by the CI Docker image build process only. Local users should
# consider https://github.com/asdf-vm/asdf to install the correct tools from the
# `.tool-versions` file.
#
# If this gets any more complicated, it might be worth considering using asdf in
# the Docker build process, but that would require setting up the sourcing of
# shell profiles.

main() {
  while read -r line; do
    tool="$(echo "$line" | awk '{print $1}')"
    version="$(echo "$line" | awk '{print $2}')"
    "install__$tool" "$version"
  done < <(grep -Ev '^#' .tool-versions | grep -Ev '^$')
}

install__go-jsonnet() {
  mkdir /tmp/go-jsonnet
  curl -L -o /tmp/go-jsonnet/go-jsonnet.tar.gz \
    "https://github.com/google/go-jsonnet/releases/download/v${version}/go-jsonnet_${version}_Linux_x86_64.tar.gz"
  tar -xf "/tmp/go-jsonnet/go-jsonnet.tar.gz" -C /tmp/go-jsonnet/

  mv /tmp/go-jsonnet/jsonnet /usr/bin/
  mv /tmp/go-jsonnet/jsonnetfmt /usr/bin/

  # TODO install jsonnet-lint and jsonnet-deps when they're available:
  # https://github.com/google/go-jsonnet/issues/480

  rm -rf /tmp/go-jsonnet
}

install__helm() {
  curl -L -o /tmp/helm.tar.gz "https://get.helm.sh/helm-v${version}-linux-amd64.tar.gz"
  mkdir -p /tmp/helm
  tar -xf /tmp/helm.tar.gz -C /tmp/helm
  mv /tmp/helm/linux-amd64/helm /usr/bin/helm
  chmod +x /usr/bin/helm
  rm -rf /tmp/helm /tmp/helm.tar.gz
}

install__jb() {
  curl -L -o /usr/bin/jb \
    "https://github.com/jsonnet-bundler/jsonnet-bundler/releases/download/v${version}/jb-linux-amd64"
  chmod +x /usr/bin/jb
}

install__kubectl() {
  curl -L -o /usr/bin/kubectl \
    "https://storage.googleapis.com/kubernetes-release/release/v${version}/bin/linux/amd64/kubectl"
  chmod +x /usr/bin/kubectl
}

install__tanka() {
  if [ -e /usr/bin/tk ]; then
    # If we're currently pinning a SHA of tanka, we don't want to clobber it
    # with a release version.
    echo "/usr/bin/tk already present, skipping tanka install"
    return 0
  fi

  curl -L -o /usr/bin/tk \
    "https://github.com/grafana/tanka/releases/download/v${version}/tk-linux-amd64"
  chmod +x /usr/bin/tk
}

main

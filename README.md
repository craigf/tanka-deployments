# tanka-deployments

A monorepo for Kubernetes deployment declarations using
[tanka](https://tanka.dev/).

[[_TOC_]]

## Getting started

1. jsonnet tutorial: https://jsonnet.org/learning/tutorial.html
1. tanka tutorial: https://tanka.dev/tutorial/overview
1. tanka readiness review: https://gitlab.com/gitlab-com/gl-infra/readiness/-/merge_requests/49

## Dependencies

1. tanka
1. jsonnet-bundler
1. kubectl
1. helm

[`.tool-versions`](.tool-versions) (used by
[`asdf`](https://github.com/asdf-vm/asdf)) should be seen as a source of truth
for versions.

This repository contains non-checked in dependencies that are managed by package
managers such as jsonnet-bundler. Run `make vendor` to ensure that your local
dependencies are up-to-date before running `tk` commands against this
repository.

## Repository structure

### Root directory

The top-level directory mainly consists of a single tanka structure created with
`tk init`.

### environments

There are two levels of tanka environments in the environments directory: the
first level contains different groups of software (e.g. thanos, prometheus), and
the second level are environments of these deployments, for example:

```
├── environments
│   ├── jaeger
│   │   ├── dev
│   │   │   ├── main.jsonnet
│   │   │   └── spec.json
│   │   ├── gstg
│   │   │   ├── main.jsonnet
│   │   │   └── spec.json
│   │   ├── pre
│   │   │   ├── main.jsonnet
│   │   │   └── spec.json
│   │   └── README.md

```

`main.jsonnet` files contain environment specific config and code. For example, `environments/jaeger/gstg/main.jsonnet` calls a number of functions from different libraries, including `lib/jaeger/jaeger.libsonnet`, and passes to them arguments specific to jaeger deployment in the gstg environment.

### lib (gitlab-created jsonnet libraries used in this project)

If there is no jsonnet-bundler managed release for a piece of software, we write the relevant jsonnet code ourselves and put it in this directory.

### vendor

External libraries installed using jsonnet-bundler.

### charts

Helm charts used by tanka's helm integration.

## Creating a new deployment / environment

```
tk env add environments/<software>/<environment> --namespace <namespace> \
  --server-from-context <kube context>
```

For example:

```
tk env add environments/thanos/gprd --namespace thanos \
  --server-from-context gprd
```

See `tk env add --help` for full details. If you don't have the relevant server
in your local kubectl context, you can pass `--server` instead to set the API
server to an arbitrary string.

It is the environment author's responsibility to ensure that the specified
namespace is created. Tanka can do this: see `namespace()` in
`lib/common.libsonnet`[lib/common.libsonnet].

You can now run `tk` eval/show/diff/apply commands as you wish for that
environment.

Ensure that `spec.injectLabels` is set to `true` in order to enable `tk prune`:
https://tanka.dev/garbage-collection.

## Ordering of k8s objects in different tk subcommands

`tk apply` sorts objects in order to support installation of resources that
depend on others in the same apply: e.g. CRDs before instances of them,
StorageClasses before PersistentVolumeClaims. `tk show` shows the order that `tk
apply` will pass to `kubectl apply`.

`tk diff` relies on `kubectl` server-side diffs, which do not support objects
that depend on others in the same list of manifests, regardless of order. Here's
the relevant tanka issue: https://github.com/grafana/tanka/issues/246.

## The dev environment

The `dev` environment for each tanka deployment is special, in that its API
server is not a globally routable shared resource. You should use this
environment to test resource configuration against a local Kubernetes
environment, e.g. minikube, k3d, or KinD.

We've chosen `localhost:16443` as the address of all dev environment's API
servers. Tanka doesn't support overriding the API server on the command line, so
you'll either have to configure your Kubernetes dev environment to use this
port, or change the environment's spec.json locally, and not commit the changed
address.

Here is a non-exhaustive list of Kubernetes development tools, and how to
configure them to use this address for the API server:

### k3d

Use a non-random API port, and replace `0.0.0.0` with a loopback address in
kubeconfig, to improve chances of compatibility with other dev environments that
can also make use of port forwarding.

```
k3d cluster create --api-port 6443 -p '127.0.0.1:16443:6443@server[0]' --no-lb \
  && kubectl config set-cluster k3d-k3s-default --server=https://127.0.0.1:16443
```

### minikube

In minikube, it's impossible to configure the binding address of `kube-api-server`. The VM's external IP address is used instead. In order to set the VM's external IP address to a static value, multiple changes would be required. A much simpler solution is to do port-forwarding:
```bash
$ minikube start --memory 20480 --cpus 5 --driver=virtualbox  # create minikube
$ vboxmanage controlvm $(vboxmanage list vms | grep minikube | awk {'print $1'} | sed 's/"//g') natpf1 "apiserver,tcp,127.0.0.1,16443,,8443"  # add port-forwarding to the VirtualBox VM network interface config
$ kubectl config set-cluster minikube --server=https://127.0.0.1:16443  # update `~/.kube/config`
```

`minikube start` will overwrite `~/.kube/config` every time you start the VM, so remember to always update the cluster's URL when you start minikube, i.e.:
```
$ minikube start
$ kubectl config set-cluster minikube --server=https://127.0.0.1:16443  # update `~/.kube/config`
```

### Fallback

```
tk env set environments/thanos/dev --server-from-context <dev context>
```

This will change the environment's `spec.json`. Please don't check in this
change.

## Secrets

At the time of writing, tanka uses `kubectl diff` (with server-side diffs) to
compare resources on diff/apply/prune, and neither tanka nor kubectl suppress
secret output (https://github.com/kubernetes/kubernetes/issues/87840).

Our deployment logs are private, but it's still desirable to avoid printing
secrets to them from tanka's output. We must therefore configure secrets out of
band. We currently manually create and edit secrets that are referenced by
tanka-managed resources.

### CI variables

If there is a Kubernetes resource field that we consider a secret that is not
actually stored in a Secret object, such as
[`Jaeger.spec.storage.options.es.server-urls`](lib/jaeger/jaeger.libsonnet), we
inject those values as CI environment variables (on our ops mirror). These are
passed to tanka as `--ext-str`, using `.gitlab-ci.jsonnet` support for
deployment-specific extra flags for tanka. These values can then be accessed
from jsonnet using `std.extVar`.

This approach is a last resort that should be avoided for two reasons:

1. These values are logged to our (private) deployment logs.
1. It complicates pipeline config.

We should aim to replace uses of this pattern with contributions to the
relevant CRDs and their controllers to be able to pull these values from
Secrets.

## Inter-deployment dependencies

Some tanka deployments may have external dependencies, referenced in
configuration or secrets. It's possible to develop a tanka deployment that
depends on another tanka deployment. There is no particular orchestration or
management of inter-deployments dependencies in this repository, and "apply"
jobs for different deployments run in parallel.

If you are creating a new deployment that cannot sensibly be deployed without a
dependency already in place, your merge request should only be merged once that
dependency is in place, whether the dependency is another tanka deployment or
not.

## CI/CD

### Pipeline configuration

This monorepo aims to only run deployment jobs when the relevant environment has
changed. The GitLab CI config required to express that is quite repetitive, and
it can be awkward to ensure that a change is made in every necessary place. To
mitigate this concern, and to improve readability, we generate our CI config
before check-in from `.gitlab-ci-jsonnet`. See the [`Makefile`](Makefile) for
how this is done. A CI job will assert that the generated code is up-to-date -
unless it's been removed from the yaml of course!

You can use this git pre-commit hook (an executable in `.git/hooks/pre-commit`)
if you like:

```
#!/usr/bin/env bash
set -euo pipefail

make generate-ci-config
if ! git diff --quiet .gitlab-ci.yml; then
  echo ".gitlab-ci.yml has changed after code generation."
  exit 1
fi
```

### Remote pipelines

This repository is public, but our pipelines run on a private mirror on
ops.gitlab.net. The main reason for this is in order to keep our deployment logs
private.

We run
[Woodhouse](https://gitlab.com/gitlab-com/gl-infra/woodhouse#gitlab-notify-mirrored-mr-subcommand)
in these mirror pipelines to post links to these remote pipelines on relevant
merge requests.

### Unattended deployments

`tk apply` and `tk prune` are usually run as manually-gated CI jobs, on master.
It is also possible to trigger a pipeline that will run only the apply step,
with no manual gating. This is done by triggering a pipeline on the ops mirror,
passing in `TANKA_DEPLOYMENTS_RUN=<environment name>`. Note that the environment
name here is the [GitLab
environment](https://docs.gitlab.com/ee/ci/environments/) name, not the tanka
environment name: for example, "jaeger-pre", or "thanos-ops".

You can see an example of this in [woodhouse's deploy
jobs](https://gitlab.com/gitlab-com/gl-infra/woodhouse/-/blob/master/.gitlab-ci.yml),
and its CI config here. That repository's deploy job triggers a
tanka-deployments pipeline using the API, passing in
`TANKA_DEPLOYMENTS_RUN=woodhouse-ops`, and also overriding that job's extra env
var `WOODHOUSE_TAG` with the image tag we're deploying, which is injected into
tanka config as an extVar.

With a combination of API-triggered pipelines, `TANKA_DEPLOYMENTS_RUN`,
and job-specific environment variables and tanka flags, you can build automatic
unattended deployment pipelines using tanka.

## Helm chart ingestion

Tanka can [template helm charts into jsonnet objects](https://tanka.dev/helm).
This is useful when deploying resources that are only distributed through helm,
and allows us to keep up with lengthy, relatively non-customizable resources
such as CRDs and Roles.

See [thanos](environments/thanos) for an example of how we currently use this
integration.

### Adding a new chart

1. If the chart's repository does not already exist in `chartfile.yaml`, add it.
1. `tk tool charts add REPO/CHART@VERSION`. This will modify `chartfile.yaml`.
1. The previous command also calls `tk tool charts vendor`, but until
   https://github.com/grafana/tanka/pull/419 is resolved this may fail. Run
   `make vendor` to reconcile the local state of the gitignored `charts`
   directory with `chartfile.yaml`.

### Updating a chart

1. Update the relevant chart's version in `chartfile.yaml`, under the `requires`
   key.
1. `make vendor`

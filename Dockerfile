FROM golang:1.16 as tanka

RUN git clone https://github.com/grafana/tanka /tanka
WORKDIR /tanka
RUN git checkout e5bf9fd
RUN CGO_ENABLED=0 make dev

FROM debian:buster

COPY --from=tanka /tanka/tk /usr/bin/tk

WORKDIR /build

RUN apt-get update && apt-get install -y \
  apt-transport-https \
  ca-certificates \
  colordiff \
  curl \
  git \
  gnupg \
  less \
  make

COPY .tool-versions /build/
COPY ./bin/install_tools /build/

RUN /build/install_tools

RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
  curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - && \
  apt-get update -y && apt-get install google-cloud-sdk -y

{
  clusters: {
    gprd: {
      type: 'regional',
      apiServer: 'https://35.243.230.38',
      googleProject: 'gitlab-production',
    },
    'gprd-us-east1-b': {
      type: 'zonal',
      apiServer: 'TODO',
      googleProject: 'gitlab-production',
    },
    'gprd-us-east1-c': {
      type: 'zonal',
      apiServer: 'TODO',
      googleProject: 'gitlab-production',
    },
    'gprd-us-east1-d': {
      type: 'zonal',
      apiServer: 'TODO',
      googleProject: 'gitlab-production',
    },
    gstg: {
      type: 'regional',
      apiServer: 'https://34.73.144.43',
      googleProject: 'gitlab-staging-1',
    },
    'gstg-us-east1-b': {
      type: 'zonal',
      apiServer: 'https://34.74.13.203',
      googleProject: 'gitlab-staging-1',
    },
    'gstg-us-east1-c': {
      type: 'zonal',
      apiServer: 'https://35.237.127.243',
      googleProject: 'gitlab-staging-1',
    },
    'gstg-us-east1-d': {
      type: 'zonal',
      apiServer: 'https://35.229.107.91',
      googleProject: 'gitlab-staging-1',
    },
    ops: {
      type: 'regional',
      apiServer: 'https://104.196.154.62',
      googleProject: 'gitlab-ops',
    },
    pre: {
      type: 'regional',
      apiServer: 'https://104.196.63.202',
      googleProject: 'gitlab-pre',
    },
    'org-ci': {
      type: 'regional',
      apiServer: 'https://35.185.28.227',
      googleProject: 'gitlab-org-ci-0d24e2',
    },
    dev: {
      type: 'dev',
      apiServer: 'https://127.0.0.1:16443',
      googleProject: 'dummy',
    },
  },

  all()::
    [
      cluster
      for cluster in std.objectFields($.clusters)
      if $.clusters[cluster].type != 'dev'
    ],

  allRegional()::
    [
      cluster
      for cluster in std.objectFields($.clusters)
      if $.clusters[cluster].type == 'regional'
    ],

  environment(cluster, namespace, release):: {
    apiVersion: 'tanka.dev/v1alpha1',
    kind: 'Environment',
    metadata: {
      name: '%s/%s' % [release, cluster],
    },
    spec: {
      apiServer: $.clusters[cluster].apiServer,
      namespace: namespace,
      injectLabels: true,
    },
  },
}

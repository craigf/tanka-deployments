local common = import 'common.libsonnet';
local tk = import 'tk';

local cluster_role = std.native('parseYaml')(importstr './cluster_role.yaml')[0];
local cluster_role_binding = std.native('parseYaml')(importstr './cluster_role_binding.yaml')[0];
local jaeger_crd = std.native('parseYaml')(importstr './jaegertracing.io_jaegers_crd.yaml')[0];
local operator = std.native('parseYaml')(importstr './operator.yaml')[0];
local role = std.native('parseYaml')(importstr './role.yaml')[0];
local role_binding = std.native('parseYaml')(importstr './role_binding.yaml')[0];
local service_account = std.native('parseYaml')(importstr './service_account.yaml')[0];

{
  jaeger_operator: {
    cluster_role: cluster_role,
    cluster_role_binding: cluster_role_binding {
      subjects: [subject { namespace: tk.env.spec.namespace } for subject in cluster_role_binding.subjects],
    },
    jaeger_crd: jaeger_crd,
    operator: operator {
      metadata+: {
        namespace: tk.env.spec.namespace,
      },
      spec+: {
        template+: {
          spec+: common.withNodePoolAffinity(),
        },
      },
    },
    role: role {
      metadata+: {
        namespace: tk.env.spec.namespace,
      },
    },
    role_binding: role_binding {
      metadata+: {
        namespace: tk.env.spec.namespace,
      },
    },
    service_account: service_account {
      metadata+: {
        namespace: tk.env.spec.namespace,
      },
    },
  },
}

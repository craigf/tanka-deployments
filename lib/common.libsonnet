local k = import 'ksonnet-util/kausal.libsonnet';

{
  // Creates a new namespace containing a "name" label, in the same way that
  // helm does. This might not be necessary but is good for consistency, and
  // avoids confusing when making label queries.
  namespace(name):: k.core.v1.namespace.new(name) + {
    metadata+: {
      labels+: {
        name: name,
      },
    },
  },

  // Wrapper for kausal.deployment.new that adds "deployment" labels to pods.
  // See withDeploymentPodLabel below for an explanation of why we do this.
  deployment(name, type, replicas, containers, podLabels={})::
    k.apps.v1.deployment.new(name, replicas, containers, podLabels)
    + self.withDeploymentPodLabel(type),

  // Adds a "deployment" label to pods, whose value is the name of the
  // deployment (or statefulset, or anything with a pod spec under
  // spec.template). We do this in order to generate container-group-level
  // metrics without having to perform gnarly promql joins, e.g.
  // https://dashboards.gitlab.net/d/git-kube-containers/git-kube-containers-detail?orgId=1&from=now-6h%2Fm&to=now%2Fm&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-sigma=2
  withDeploymentPodLabel(type, deployment=null)::
    {
      local resource = self,
      spec+: {
        template+: {
          metadata+: {
            labels+: {
              type: type,
              deployment: if deployment == null then resource.metadata.name else deployment,
            },
          },
        },
      },
    },

  managedCert(name, domain):: {
    apiVersion: 'networking.gke.io/v1beta2',
    kind: 'ManagedCertificate',
    metadata: {
      name: name,
    },
    spec: {
      domains: [domain],
    },
  },

  backendConfig(name, iapSecretName=null):: {
    apiVersion: 'cloud.google.com/v1beta1',
    kind: 'BackendConfig',
    metadata: {
      name: name,
    },
    spec: {
      iap: {
        enabled: if iapSecretName != null then true else false,
        oauthclientCredentials: {
          secretName: iapSecretName,
        },
      },
    },
  },

  // Useful for patching individual containers in a pod spec.
  // Credit to https://stackoverflow.com/a/61500569 for the idea to convert
  // between maps and arrays to work around jsonnet's lack of flexibility in
  // manipulating arrays.
  patchElementByName(patch, name, arr)::
    local elementMap = { [e.name]: e for e in arr };
    local patchedMap = elementMap {
      [name]: super[name] + patch,
    };
    [{ name: e } + patchedMap[e] for e in std.objectFields(patchedMap)],

  externalURL(hostname, env, fqdn=false)::
    '%s.%s.gke.gitlab.net%s' % [hostname, env, if fqdn then '.' else ''],

  // Our GKE clusters have some node pools that are supposed to be reserved for
  // particular workloads, such as various sidekiq priority classes. These are
  // not tainted, so any workload can run on them. Until this is resolved
  // (https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10688), it
  // might be desirable to prevent large workloads from running on anything but
  // the default node pool.
  //
  // Mix into a Pod.spec, or a pod template (e.g. a
  // Deployment.spec.template.spec)
  withNodePoolAffinity(nodePool='default'):: {
    affinity+: {
      nodeAffinity: {
        requiredDuringSchedulingIgnoredDuringExecution: {
          nodeSelectorTerms: [{
            matchExpressions: [{
              key: 'type',
              operator: 'In',
              values: [nodePool],
            }],
          }],
        },
      },
    },
  },

  standardMetricLabels(type, stage, shard=null):: [
    {
      targetLabel: 'type',
      replacement: type,
    },
    {
      targetLabel: 'stage',
      replacement: stage,
    },
    {
      sourceLabels: ['__meta_kubernetes_pod_node_name'],
      targetLabel: 'node',
    },
  ] + (
    if shard == null then [] else [
      {
        targetLabel: 'shard',
        replacement: shard,
      },
    ]
  ),

  toBytes(sizeStr)::
    local validInput = std.native('regexMatch')('^\\d+[a-zA-Z]+$', sizeStr);
    assert validInput : '"%s" is not a valid byte size expression' % sizeStr;

    local scalar = std.parseInt(std.native('regexSubst')('[a-zA-Z]+', sizeStr, ''));
    local unit = std.native('regexSubst')('\\d+', sizeStr, '');
    if unit == 'Gi' then
      scalar * 1024 * 1024 * 1024
    else if unit == 'Mi' then
      scalar * 1024 * 1024
    else if unit == 'Ki' then
      scalar * 1024
    else if unit == 'B' then
      scalar
    else
      assert false : '"%s" is not a recognized unit' % unit;
      0,
}

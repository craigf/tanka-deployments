local k = (import 'ksonnet-util/kausal.libsonnet') + {
  _config+:: {
    namespace: 'evicted-pod-reaper',
  },
};

local cronjob = k.batch.v1beta1.cronJob;
local container = k.core.v1.container;

local _config = {
  image: 'registry.gitlab.com/gitlab-com/gl-infra/k8s-workloads/common/k8-helm-ci:latest',
  command: ['sh', '-c', 'kubectl -n gitlab get pods -o json | jq \'.items[] | select(.status.reason == "Evicted").metadata.name\' | xargs -r kubectl -n gitlab delete pod'],
  rbacRules: [
    {
      apiGroups: [''],
      resources: ['pods'],
      verbs: ['get', 'list', 'delete'],
    },
  ],
};

{
  new(name='evicted-pod-reaper', schedule='*/30 * * * *'):: {
    cronJob: cronjob.new(
      name=name,
      schedule=schedule,
      containers=[container.new(
        name=name,
        image=_config.image,
      ) + {
        command: _config.command,
      }],
    ) + {
      spec+: {
        jobTemplate+: {
          spec+: {
            template+: {
              spec+: {
                restartPolicy: 'Never',
                serviceAccountName: name,
              },
            },
          },
        },
      },
    },

    rbac: k.util.rbac(name=name, rules=_config.rbacRules),

  },
}

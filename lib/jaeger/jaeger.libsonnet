local k = import 'ksonnet-util/kausal.libsonnet';

{
  instance:: {
    new(
      dependencies_enabled=false,
      dependencies_resources={},
      es_index_prefix='jaeger',
      es_server_urls,
      es_pass_secret_name='jaeger-secret',
      es_cert_secret_name='es-http-certs-public',
      name='jaeger',
      resources={},
      sampling_strategy_param=0.1,
      sampling_strategy_type='probabilistic',
    ):: {
      apiVersion: 'jaegertracing.io/v1',
      kind: 'Jaeger',
      metadata: {
        name: name,
      },
      spec: {
        agent: {
          strategy: 'DaemonSet',
        },
        collector: {
          maxReplicas: 5,
          resources: resources,
        },
        query: {},
        ingress: {
          enabled: false,
        },
        labels: {
          stage: 'main',
          tier: 'inf',
          type: 'jaeger',
          shard: 'default',
        },
        sampling: {
          options: {
            default_strategy: {
              param: sampling_strategy_param,
              type: sampling_strategy_type,
            },
          },
        },
        storage: {
          options: {
            es: {
              'server-urls': es_server_urls,
              'index-prefix': es_index_prefix,
              tls: {
                ca: '/es/certificates/ca.crt',
              },
            },
          },
          secretName: es_pass_secret_name,
          type: 'elasticsearch',
          dependencies: {
            enabled: dependencies_enabled,
            schedule: '55 23 * * *',
            sparkMaster: '',
            resources: dependencies_resources,
            javaOpts: '-XX:MinRAMPercentage=50.0 -XX:MaxRAMPercentage=80.0',
          },
        },
        strategy: 'production',
        volumeMounts: [
          {
            name: 'certificates',
            mountPath: '/es/certificates/',
            readOnly: true,
          },
        ],
        volumes: [
          {
            name: 'certificates',
            secret: {
              secretName: es_cert_secret_name,
            },
          },
        ],
      },
    },
  },

  // jaeger-operator doesn't support disabling the service, so this one is created in addition to the jaeger-operator managed one
  serviceQuery:: {
    new(
      instance_name='jaeger',
      annotations={},
    )::
      k.core.v1.service.new(
        name=instance_name + '-query-tanka-managed',
        selector={
          app: 'jaeger',
          'app.kubernetes.io/component': 'query',
          'app.kubernetes.io/instance': instance_name,
          'app.kubernetes.io/managed-by': 'jaeger-operator',
          'app.kubernetes.io/name': instance_name + '-query',
          'app.kubernetes.io/part-of': 'jaeger',
        },
        ports=[
          {
            name: 'query',
            port: 16686,
            protocol: 'TCP',
            targetPort: 'query',
          },
        ],
      ) + {
        metadata+: {
          annotations+: annotations,
        },
        spec+: {
          type: 'NodePort',
        },
      },
  },

  // jaeger-operator doesn't support disabling the service, so this one is created in addition to the jaeger-operator managed one
  serviceCollector:: {
    new(
      instance_name='jaeger',
      annotations={},
    )::
      k.core.v1.service.new(
        name=instance_name + '-collector-tanka-managed',
        selector={
          app: 'jaeger',
          'app.kubernetes.io/component': 'collector',
          'app.kubernetes.io/instance': instance_name,
          'app.kubernetes.io/managed-by': 'jaeger-operator',
          'app.kubernetes.io/name': instance_name + '-collector',
          'app.kubernetes.io/part-of': 'jaeger',
        },
        ports=[
          {
            name: 'zipkin',
            port: 9411,
            protocol: 'TCP',
            targetPort: 'zipkin',
          },
          {
            name: 'grpc',
            port: 14250,
            protocol: 'TCP',
            targetPort: 'grpc',
          },
          {
            name: 'c-tchan-trft',
            port: 14267,
            protocol: 'TCP',
            targetPort: 'c-tchan-trft',
          },
          {
            name: 'c-binary-trft',
            port: 14268,
            protocol: 'TCP',
            targetPort: 'c-binary-trft',
          },
        ],
      ) + {
        metadata+: {
          annotations+: annotations,
        },
        spec+: {
          type: 'LoadBalancer',
        },
      },
  },

  // jaeger-operator doesn't support changing the backend service used by the ingress
  ingressQuery(
    instance_name='jaeger',
    ingress_enabled=false,
    ingress_annotations={},
  ):: {
    apiVersion: 'extensions/v1beta1',
    kind: 'Ingress',
    metadata: {
      name: 'jaeger-query-tanka-managed',
      annotations: ingress_annotations,
    },
    spec: {
      backend: {
        serviceName: instance_name + '-query-tanka-managed',
        servicePort: 'query',
      },
    },
  },

  podMonitorQuery:: {
    apiVersion: 'monitoring.coreos.com/v1',
    kind: 'PodMonitor',
    metadata: {
      name: 'jaeger-query',
    },
    spec: {
      podMetricsEndpoints: [
        {
          interval: '15s',
          path: '/metrics',
          port: 'admin-http',
          relabelings: [
            {
              action: 'drop',
              regex: '^false$',
              sourceLabels: [
                '__meta_kubernetes_pod_ready',
              ],
            },
            {
              action: 'labelmap',
              regex: '__meta_kubernetes_pod_label_(.+)',
            },
          ],
        },
      ],
      selector: {
        matchLabels: {
          'app.kubernetes.io/component': 'query',
          'app.kubernetes.io/instance': 'jaeger',
          'app.kubernetes.io/managed-by': 'jaeger-operator',
          'app.kubernetes.io/name': 'jaeger-query',
          'app.kubernetes.io/part-of': 'jaeger',
        },
      },
    },
  },

  podMonitorCollector:: {
    apiVersion: 'monitoring.coreos.com/v1',
    kind: 'PodMonitor',
    metadata: {
      name: 'jaeger-collector',
    },
    spec: {
      podMetricsEndpoints: [
        {
          interval: '15s',
          path: '/metrics',
          port: 'admin-http',
          relabelings: [
            {
              action: 'drop',
              regex: '^false$',
              sourceLabels: [
                '__meta_kubernetes_pod_ready',
              ],
            },
            {
              action: 'labelmap',
              regex: '__meta_kubernetes_pod_label_(.+)',
            },
          ],
        },
      ],
      selector: {
        matchLabels: {
          'app.kubernetes.io/component': 'collector',
          'app.kubernetes.io/instance': 'jaeger',
          'app.kubernetes.io/managed-by': 'jaeger-operator',
          'app.kubernetes.io/name': 'jaeger-collector',
          'app.kubernetes.io/part-of': 'jaeger',
        },
      },
    },
  },

  podMonitorAgent:: {
    apiVersion: 'monitoring.coreos.com/v1',
    kind: 'PodMonitor',
    metadata: {
      name: 'jaeger-agent',
    },
    spec: {
      podMetricsEndpoints: [
        {
          interval: '15s',
          path: '/metrics',
          port: 'admin-http',
          relabelings: [
            {
              action: 'drop',
              regex: '^false$',
              sourceLabels: [
                '__meta_kubernetes_pod_ready',
              ],
            },
            {
              action: 'labelmap',
              regex: '__meta_kubernetes_pod_label_(.+)',
            },
          ],
        },
      ],
      selector: {
        matchLabels: {
          'app.kubernetes.io/component': 'agent',
          'app.kubernetes.io/instance': 'jaeger',
          'app.kubernetes.io/managed-by': 'jaeger-operator',
          'app.kubernetes.io/name': 'jaeger-agent',
          'app.kubernetes.io/part-of': 'jaeger',
        },
      },
    },
  },

}

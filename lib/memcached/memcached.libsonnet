local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);
local common = import 'common.libsonnet';

local serverMemoryOverheadMbytes = 50;

{
  new(name, namespace, type, nodePool=null, chartValues={}, metricRelabelings=[])::
    local memBytes = common.toBytes(chartValues.resources.requests.memory);
    local memMBytes = memBytes / 1024 / 1024;
    assert memMBytes > serverMemoryOverheadMbytes : |||
      %dMB is not enough memory to run memcached - you'll need at least %dMB
      plus the space you want for the cache.
    ||| % [memMBytes, serverMemoryOverheadMbytes];
    local cacheSizeMB = memMBytes - serverMemoryOverheadMbytes;

    // This provisions a Secret containing the memcached password, which is not
    // really a Secret because we use empty string. We are not using auth for
    // memcached, and are relying the fact that we don't expose it to the
    // internet to secure it.
    // If we want to use a proper password, we should patch out the Secret to
    // null, and document the structure of the secret to be manually configured
    // pre-deployment.
    local defaultChartValues = {
      nameOverride: name,
      architecture: 'high-availability',
      persistence: {
        enabled: false,
      },
      metrics: {
        enabled: true,
        serviceMonitor: {
          enabled: true,
        },
      },

      // extraEnv is an array, but we will work with objects to enable patching
      // semantics, with fallback to default values, before converting back to
      // an array.
      extraEnv: {
        MEMCACHED_EXTRA_FLAGS: '--max-item-size=16M',
        MEMCACHED_CACHE_SIZE: '%d' % cacheSizeMB,
      },
    };

    local mergedChartValues = defaultChartValues + chartValues;
    local mergedChartValuesWithEnv = mergedChartValues {
      extraEnv: [{ name: key, value: mergedChartValues.extraEnv[key] } for key in std.objectFields(mergedChartValues.extraEnv)],
    };

    local resources = helm.template(
      'memcached',
      '../../charts/memcached',
      { values: mergedChartValuesWithEnv, namespace: namespace },
    );
    resources {
      [key]: super[key] + (
        if std.startsWith(key, 'stateful_set') then
          common.withDeploymentPodLabel(type) + (
            if nodePool != null then {
              spec+: {
                template+: {
                  spec+: common.withNodePoolAffinity(nodePool),
                },
              },
            }
            else {}
          )
        else if std.startsWith(key, 'service_monitor') then {
          spec+: {
            endpoints: [
              endpoint {
                relabelings+: metricRelabelings,
              }
              for endpoint in super.endpoints
            ],
          },
        }
        else {}
      )
      for key in std.objectFields(resources)
    },
}

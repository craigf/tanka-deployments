local stages = {
  stages: [
    'notify',
    'build_ci_image',
    'checks',
    'diff',
    'apply',
  ],
};

// Prevent duplicate detached merge request pipelines from being created
// https://docs.gitlab.com/ee/ci/yaml/README.html#prevent-duplicate-pipelines
local workflow = {
  workflow: {
    rules: [
      {
        'if': '$CI_PIPELINE_SOURCE == "merge_request_event"',
        when: 'never',
      },
      {
        when: 'always',
      },
    ],
  },
};

local mainRegion = 'us-east1';

local clusterAttrs = {
  pre: {
    GOOGLE_PROJECT: 'gitlab-pre',
    GKE_CLUSTER: 'pre-gitlab-gke',
    GOOGLE_REGION: mainRegion,
  },

  gstg: {
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gstg-gitlab-gke',
    GOOGLE_REGION: mainRegion,
  },
  'gstg-us-east1-b': {
    ENVIRONMENT: 'gstg',
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gstg-us-east1-b',
    GOOGLE_ZONE: 'us-east1-b',
  },
  'gstg-us-east1-c': {
    ENVIRONMENT: 'gstg',
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gstg-us-east1-c',
    GOOGLE_ZONE: 'us-east1-c',
  },
  'gstg-us-east1-d': {
    ENVIRONMENT: 'gstg',
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gstg-us-east1-d',
    GOOGLE_ZONE: 'us-east1-d',
  },

  gprd: {
    GOOGLE_PROJECT: 'gitlab-production',
    GKE_CLUSTER: 'gprd-gitlab-gke',
    GOOGLE_REGION: mainRegion,
  },

  ops: {
    GOOGLE_PROJECT: 'gitlab-ops',
    GKE_CLUSTER: 'ops-gitlab-gke',
    GOOGLE_REGION: mainRegion,
  },

  'org-ci': {
    GOOGLE_PROJECT: 'gitlab-org-ci-0d24e2',
    GKE_CLUSTER: 'org-ci-gitlab-gke',
    GOOGLE_REGION: mainRegion,
  },
};

local ruleEnableDeployments = '$TANKA_DEPLOYMENTS_RUN == "1"';
local ruleOutsideDeploymentPipeline = '$TANKA_DEPLOYMENTS_RUN == null';
local ruleOnMaster = '$CI_COMMIT_BRANCH == "master"';
local ruleManual(cluster) =
  local autoDeployEnvs = [
    'pre',
    'gstg',
    'gstg-us-east1-b',
    'gstg-us-east1-c',
    'gstg-us-east1-d',
  ];
  if std.length(std.find(cluster, autoDeployEnvs)) > 0 then
    {}
  else
    { when: 'manual' };

local ruleAnd(rule) =
  ' && %s' % rule;

local ruleOr(rule) =
  ' || %s' % rule;

local ruleChanges(changes) =
  { changes: changes };

local tankaInlineJobs(software, cluster, tkExtraArgs='', extraVars={}, diffNotes='') =
  local envDir = 'environments/%s' % software;
  local envName = '%s/%s' % [software, cluster];

  // Run jobs for this environment when the environment changes, but also when
  // any of the generic code changes: lib, jsonnet-bundler deps, CI config. Err
  // on the side of running jobs when something has changed that they might
  // depend on.
  local envChanged = ruleChanges(['%s/*' % envDir, 'lib/**/*', 'jsonnetfile.*', '.gitlab-ci.yml', 'Dockerfile']);

  local baseJob = {
    image: '${CI_REGISTRY_IMAGE}/ci:latest',

    // run on ops runners with access to our cluster VPCs
    tags: ['k8s-workloads'],

    variables: {
      ENVIRONMENT: cluster,
    } + clusterAttrs[cluster] + extraVars,
  };
  {
    ['%s:%s:diff' % [software, cluster]]: baseJob {
      stage: 'diff',
      script: |||
        ./bin/kubectl_login
        make vendor

        tk diff --with-prune --exit-zero %(envDir)s --name %(envName)s %(tkExtraArgs)s | colordiff
        echo -e "\033[0;31m%(diffNotes)s\033[0m"
      ||| % {
        envDir: envDir,
        envName: envName,
        tkExtraArgs: tkExtraArgs,
        diffNotes: diffNotes,
      },
      rules: [
        { 'if': ruleEnableDeployments } + envChanged,
        { 'if': '$TANKA_DEPLOYMENTS_RUN == "%s"' % envName },
      ],
    },

    ['%s:%s:apply' % [software, cluster]]: baseJob {
      stage: 'apply',
      needs: ['%s:%s:diff' % [software, cluster]],
      environment: envName,
      resource_group: envName,
      script: |||
        ./bin/kubectl_login
        make vendor
        tk apply --dangerous-auto-approve %(envDir)s --name %(envName)s %(tkExtraArgs)s | colordiff
        tk prune --dangerous-auto-approve %(envDir)s --name %(envName)s %(tkExtraArgs)s | colordiff
      ||| % {
        envDir: envDir,
        envName: envName,
        tkExtraArgs: tkExtraArgs,
      },
      rules: [
        { 'if': ruleEnableDeployments + ruleAnd(ruleOnMaster) } + envChanged + ruleManual(cluster),
        { 'if': '$TANKA_DEPLOYMENTS_RUN == "%s"' % envName },
      ],
    },
  };

local tankaStaticJobs(software, cluster, tkExtraArgs='', extraVars={}, diffNotes='') =
  local envSlug = '%s-%s' % [software, cluster];
  local envDir = 'environments/%s/%s' % [software, cluster];

  // Run jobs for this environment when the environment changes, but also when
  // any of the generic code changes: lib, jsonnet-bundler deps, CI config. Err
  // on the side of running jobs when something has changed that they might
  // depend on.
  local envChanged = ruleChanges(['%s/*' % envDir, 'lib/**/*', 'jsonnetfile.*', '.gitlab-ci.yml', 'Dockerfile']);

  local baseJob = {
    image: '${CI_REGISTRY_IMAGE}/ci:latest',

    // run on ops runners with access to our cluster VPCs
    tags: ['k8s-workloads'],

    variables: {
      ENVIRONMENT: cluster,
      TANKA_ENV: envDir,
    } + clusterAttrs[cluster] + extraVars,
  };

  // Rules clauses can be a lttle hard to read. Each rule in the array is OR'ed:
  // if any pass, the job will run according to that rule's "when" value. Within
  // each rule, the various clauses ('if', 'changes') are AND'ed. In this way
  // it's possible to express complex rules such as "if $FOO is set and the
  // branch is master, run the job with manual gating - but if $BAR is set, run
  // it automatically with no manual intervention."
  //
  // Read the generated rules carefully, until you are sure what effect they
  // will have in a CI pipeline - even a branch run.
  //
  // https://docs.gitlab.com/ee/ci/yaml/
  {
    ['%s:%s:diff' % [software, cluster]]: baseJob {
      stage: 'diff',
      script: |||
        ./bin/kubectl_login
        make vendor

        tk diff --with-prune --exit-zero "${TANKA_ENV}" %(tkExtraArgs)s | colordiff
        echo -e "\033[0;31m%(diffNotes)s\033[0m"
      ||| % { tkExtraArgs: tkExtraArgs, diffNotes: diffNotes },
      rules: [
        { 'if': ruleEnableDeployments } + envChanged,
        { 'if': '$TANKA_DEPLOYMENTS_RUN == "%s"' % envSlug },
      ],
    },

    // The apply job has an extra rule that allows environments to be deployed
    // without manual intervention. This is designed for auto-deploy pipelines
    // in other repositories, that could trigger a tanka-deployments pipeline
    // via the API, passing in the env slug and also possibly other vars such as
    // Docker image tags.
    // This job has a rule that allows environments to be deployed without
    // manual intervention, but is not run in "normal" commit-triggered
    // pipelines where we set TANKA_DEPLOYMENTS_RUN=1. This is designed for
    // auto-deploy pipelines in other repositories, that could trigger a
    // tanka-deployments pipeline via the API, passing in the env slug and also
    // possibly other vars such as Docker image tags.
    // We must split it from the regular apply job to avoid using a "needs"
    // keyword that references jobs that won't be instantiated in such
    // pipelines.
    // If the only thing being bumped is an image tag, there is less benefit in
    // reviewing tanka diffs - the code change has already been approved. This
    // is in contrast with modifications to the tanka manifests themselves,
    // which should always be reviewed.
    ['%s:%s:apply' % [software, cluster]]: baseJob {
      stage: 'apply',
      needs: ['%s:%s:diff' % [software, cluster]],
      environment: envSlug,
      resource_group: envSlug,
      script: |||
        ./bin/kubectl_login
        make vendor
        tk apply "${TANKA_ENV}" --dangerous-auto-approve %(tkExtraArgs)s | colordiff
        tk prune "${TANKA_ENV}" --dangerous-auto-approve %(tkExtraArgs)s | colordiff
      ||| % { tkExtraArgs: tkExtraArgs },
      rules: [
        { 'if': ruleEnableDeployments + ruleAnd(ruleOnMaster) } + envChanged + ruleManual(cluster),
        { 'if': '$TANKA_DEPLOYMENTS_RUN == "%s"' % envSlug },
      ],
    },
  };

local buildCIDockerImageJob = {
  build_ci_docker_image: {
    stage: 'build_ci_image',
    image: 'docker:stable',
    services: ['docker:dind'],
    script: |||
      docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
      tag="${CI_REGISTRY_IMAGE}/ci:latest"
      docker build -t "${tag}" .
      docker push "${tag}"
    |||,
    rules: [
      // Run this job on master commits but not when TANKA_DEPLOYMENTS_RUN is
      // set to some value other than "1" - basically, don't run this job when
      // we're in an API-triggered pipelined intended to auto-deploy one thing.
      // Alternatively, run this job when BUILD_CI_IMAGE is set to 1, which
      // only happens in manually triggered pipelines and in any case not often.
      { 'if': ruleOnMaster + ruleAnd('(%s)' % (ruleEnableDeployments + ruleOr(ruleOutsideDeploymentPipeline))) }
      + ruleChanges(['Dockerfile', '.tool-versions', 'bin/install_tools']),
      { 'if': '$BUILD_CI_IMAGE == "1"' },
    ],
  },
};

local notifyMirrorSourceMR = {
  notify_mirror_source: {
    stage: 'notify',
    image: 'registry.gitlab.com/gitlab-com/gl-infra/woodhouse:latest',
    script: 'woodhouse gitlab notify-mirrored-mr',
    rules: [
      { 'if': ruleEnableDeployments },
    ],
  },
};

local assertFormatting = {
  assert_formatting: {
    stage: 'checks',
    image: '${CI_REGISTRY_IMAGE}/ci:latest',
    script: |||
      find . -name '*.*sonnet' | xargs -n1 jsonnetfmt -i
      git diff --exit-code
    |||,
    rules: [
      { 'if': ruleEnableDeployments + ruleOr(ruleOutsideDeploymentPipeline) },
    ],
  },
};

local ciConfigGenerated = {
  ci_config_generated: {
    stage: 'checks',
    image: '${CI_REGISTRY_IMAGE}/ci:latest',
    script: |||
      make generate-ci-config
      git diff --exit-code
    |||,
    rules: [
      { 'if': ruleEnableDeployments + ruleOr(ruleOutsideDeploymentPipeline) },
    ],
  },
};

local explanation = '# Generated by `make generate-ci-config` - do not hand edit!\n\n';

local woodhouseExtVar = '--tla-str tag=${WOODHOUSE_TAG}';
local woodhouseVars = { WOODHOUSE_TAG: 'latest' };
local woodhouseDiffNotes = |||
  !!! Attention !!!
  This environment is often applied via API-triggered pipelines that override
  the image tag. If you're seeing a diff in that field, that's probably why. It
  should be safe to deploy the "latest" tag, due to ImagePullPolicy=Always.
  !!! Attention !!!
|||;

local gitlabCIConf = stages
                     + workflow
                     + notifyMirrorSourceMR
                     + assertFormatting
                     + ciConfigGenerated
                     + tankaStaticJobs('jaeger', 'pre', '--ext-str jaeger_es_urls=${JAEGER_ES_URLS_PRE}')
                     + tankaStaticJobs('jaeger', 'gstg', '--ext-str jaeger_es_urls=${JAEGER_ES_URLS_GSTG}')
                     + tankaInlineJobs('thanos', 'pre')
                     + tankaInlineJobs('thanos', 'gstg')
                     + tankaInlineJobs('thanos', 'gprd')
                     + tankaInlineJobs('thanos', 'org-ci')
                     + tankaInlineJobs('thanos', 'ops')

                     + tankaInlineJobs(
                       'woodhouse',
                       'gstg',
                       woodhouseExtVar,
                       woodhouseVars,
                       woodhouseDiffNotes,
                     )

                     + tankaInlineJobs(
                       'woodhouse',
                       'ops',
                       woodhouseExtVar,
                       woodhouseVars,
                       woodhouseDiffNotes,
                     )

                     + tankaStaticJobs('evicted-pod-reaper', 'gprd')
                     + buildCIDockerImageJob;

explanation + std.manifestYamlDoc(gitlabCIConf)

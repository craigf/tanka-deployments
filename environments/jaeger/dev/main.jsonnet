local common = import 'common.libsonnet';
local jaeger = import 'jaeger/jaeger.libsonnet';
local tk = import 'tk';


(import 'elasticsearch-operator/eck_operator_all_in_one.libsonnet') +
(import 'jaeger/dev_es_cluster.libsonnet') +
(import 'jaeger-operator/jaeger-operator.libsonnet') +
{
  local instance_name = 'jaeger-dev',
  jaeger+: {
    namespace: common.namespace(name=tk.env.spec.namespace),
    instance: jaeger.instance.new(
      es_index_prefix='jaeger-dev',
      es_server_urls='http://quickstart-es-http.jaeger.svc:9200',
      es_pass_secret_name='jaeger-secret',
      es_cert_secret_name='quickstart-es-http-certs-public',
      name=instance_name,
    ),
    serviceQuery: jaeger.serviceQuery.new(
      instance_name=instance_name,
    ),
    serviceCollector: jaeger.serviceCollector.new(
      instance_name=instance_name,
    ),
  },
}

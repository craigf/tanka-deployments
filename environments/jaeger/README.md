
[[_TOC_]]


# Useful links #

Useful links:
- Managed cert in GKE (using ingress annotation): https://cloud.google.com/kubernetes-engine/docs/how-to/managed-certs#creating_an_ingress_with_a_managed_certificate
- IAP for GKE: https://cloud.google.com/iap/docs/enabling-kubernetes-howto
- binding BackendConfig with a service in GKE: https://cloud.google.com/kubernetes-engine/docs/how-to/ingress-features#associating_backendconfig_with_your_ingress
- BackendConfig for IAP: https://cloud.google.com/kubernetes-engine/docs/how-to/ingress-features#iap

# ES #

## index ##

Index in the ES is created when the first span is sent to Jaeger, not when Jaeger successfully connects to the cluster on startup.

## ES user permissions

Permissions required for the ES user used by Jaeger instances can be found here: https://github.com/jaegertracing/jaeger/issues/1691

# Deploying #

## Terraform ##

Some resources are managed out-of-band via terraform. This includes:

* IP address for the publically available jaeger-query service (`google_compute_global_address`)
* DNS records

An example MR adding those resources: https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/-/merge_requests/2131.

## Namespace ##

The `jaeger` namespace needs to be manually created out-of-band.

```
$ kubectl create namespace jaeger
```

## Secrets ##

### ES URLS ###

Jaeger environments deployed from CI require the ES urls to be passed from environment variables. For details, e.g. names of the vars to use, see CI config and lib/jaeger.libsonnet.

### ES cert ###

One way to create the `es-cert` secret:
```
$ CERT=$(openssl s_client -showcerts -connect <es_host_without_protocol>:<es_port> </dev/null 2>/dev/null | sed --quiet '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'); kubectl -n jaeger create secret generic es-cert --from-literal=ca.crt=$CERT
```

### ES pass ###

One way to create the `es-pass` secret:
```
$ echo $PASSWORD  # get your password into an env var, if a user already exists the password for it should be in 1pass
$ kubectl -n jaeger create secret generic es-pass --from-literal=ES_PASSWORD="$PASSWORD" --from-literal=ES_USERNAME=<elastic_username>
```

### OAuth for IAP ###

Using gcloud web console read an OAuth client ID under OAuth => Credentials. You can use the existing `monitoring` ID. Start by downloading the credentials JSON.

One way to create the oauth secret:
```
$ CLIENT_ID="$(cat client_secret_<name>.json | jq -r '.web.client_id')"
$ CLIENT_SECRET="$(cat client_secret_<name>.json | jq -r '.web.client_secret')"
$ echo $CLIENT_ID $CLIENT_SECRET
$ kubectl -n jaeger create secret generic jaeger-oauth-secret --from-literal=client_id=$CLIENT_ID --from-literal=client_secret=$CLIENT_SECRET
```

You'll also need to add your domain to the list of paths in the `monitoring` ID config and give the relevant group in GCP IAM (infra team) permissions to access the service (see: https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11560#note_429045780). At the moment of writing both of these are not terraform managed.

# Development #

## Deploying ##

1. Run `tk apply` multiple times until you get a clean run (there's a bug in Tanka that results in errors if the CRD is created in the same `tk apply` run as the "instance" of this object type).
1. Copy accross password from an ES secret to a Jaeger secret: `ES_PASSWORD=$(kubectl -n jaeger get secret quickstart-es-elastic-user -o json | jq -r ".data.elastic | @base64d"); kubectl -n jaeger create secret generic jaeger-secret --from-literal=ES_PASSWORD="$ES_PASSWORD" --from-literal=ES_USERNAME=elastic`

## Confirm jaeger is working ##

1. Deploy Prometheus-operator bundle:
```
$ kubectl apply -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/master/bundle.yaml
```
1. Create a Prometheus instance:
```
$ cat <<EOF | kubectl apply -f -
apiVersion: monitoring.coreos.com/v1
kind: Prometheus
metadata:
  name: prometheus
  namespace: jaeger
spec:
  serviceMonitorSelector:
    matchLabels:
      team: frontend
  resources:
    requests:
      memory: 400Mi
  containers:
    - name: prometheus
      env:
        - name: "JAEGER_AGENT_HOST"
          valueFrom:
            fieldRef:
              fieldPath: status.hostIP
        - name: "JAEGER_AGENT_PORT"
          value: "6831"
        - name: "JAEGER_SAMPLER_TYPE"
          value: "probabilistic"
        - name: "JAEGER_SAMPLER_PARAM"
          value: "1"
        - name: "JAEGER_DISABLED"
          value: "false"
EOF
```
1. Recreate agent and collector pods (to be investigated further why this is needed): `kubectl -n jaeger delete pod <jaeger_agent_daemonset_pod_name>; kubectl -n jaeger delete pod <jaeger_collector_pod_name>`
1. Deploy Kibana:
```
$ cat <<EOF | kubectl apply -f -
apiVersion: kibana.k8s.elastic.co/v1
kind: Kibana
metadata:
  name: quickstart
  namespace: jaeger
spec:
  version: 7.9.1
  count: 1
  elasticsearchRef:
    name: quickstart
EOF
```

local common = import 'common.libsonnet';
local jaeger = import 'jaeger/jaeger.libsonnet';
local tk = import 'tk';

// extvars
local jaeger_es_urls = std.extVar('jaeger_es_urls');

(import 'jaeger-operator/jaeger-operator.libsonnet') +
{
  local instance_name = 'jaeger',
  jaeger: {
    namespace: common.namespace(name=tk.env.spec.namespace),
    instance: jaeger.instance.new(
      es_index_prefix='jaeger-gstg',
      es_server_urls=jaeger_es_urls,
      es_pass_secret_name='es-pass',
      es_cert_secret_name='es-cert',
      name=instance_name,
      resources={
        limits: {
          cpu: '1',
          memory: '256Mi',
        },
        requests: {
          cpu: '1',
          memory: '256Mi',
        },
      },
    ) + {
      spec+: {
        collector+: common.withNodePoolAffinity(),
        query+: common.withNodePoolAffinity(),
      },
    },
    serviceQuery: jaeger.serviceQuery.new(
      instance_name=instance_name,
      annotations={
        'cloud.google.com/backend-config': std.manifestJsonEx({ default: instance_name }, ''),
      },
    ),
    serviceCollector: jaeger.serviceCollector.new(
      instance_name=instance_name,
      annotations={
        'cloud.google.com/load-balancer-type': 'Internal',
      },
    ),
    managedCert: common.managedCert(
      name=instance_name,
      domain='jaeger.gstg.gitlab.net'
    ),
    backendConfig: common.backendConfig(
      name=instance_name,
      iapSecretName='jaeger-oauth-secret'
    ),
    ingressQuery: jaeger.ingressQuery(
      instance_name=instance_name,
      ingress_enabled=true,
      ingress_annotations={
        'kubernetes.io/ingress.global-static-ip-name': 'jaeger-iap-gstg',
        'kubernetes.io/ingress.allow-http': 'false',
        'networking.gke.io/managed-certificates': 'jaeger',
      },
    ),
    podMonitorQuery: jaeger.podMonitorQuery,
    podMonitorCollector: jaeger.podMonitorCollector,
    podMonitorAgent: jaeger.podMonitorAgent,
  },
}

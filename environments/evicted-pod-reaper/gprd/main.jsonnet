local common = import 'common.libsonnet';
local evictedPodReaper = import 'evicted-pod-reaper/evicted-pod-reaper.libsonnet';
local tk = import 'tk';

{
  evictedPodReaper: {
    namespace: common.namespace(tk.env.spec.namespace),
    cronJob: evictedPodReaper.new(),
  },
}

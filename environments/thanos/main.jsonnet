local clusters = import 'clusters.libsonnet';
local environments = import 'environments.libsonnet';
local query = import 'query.libsonnet';
local store = import 'store.libsonnet';

local namespace = 'monitoring';

{
  [env]: clusters.environment(env, namespace, 'thanos') + {
    data: {
      store: store.new(env, namespace),
    } + (
      if std.objectHas(environments[env], 'query') then {
        query: query.new(env, namespace) + (
          if std.objectHas(environments[env].queryFrontend, 'ingress') then
            local ingress = environments[env].queryFrontend.ingress;
            query.withIngress(ingress.ipName, ingress.domain)
          else {}
        ),
      }
      else {}
    ),
  }
  for env in std.objectFields(environments)
}

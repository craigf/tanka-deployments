local clusters = import 'clusters.libsonnet';
local common = import 'common.libsonnet';
local thanosConfig = import 'config.libsonnet';
local environments = import 'environments.libsonnet';
local k = import 'ksonnet-util/kausal.libsonnet';
local thanos = import 'kube-thanos/thanos.libsonnet';
local memcached = import 'memcached/memcached.libsonnet';

{
  new(env, namespace)::
    local envConfig = environments[env].store;

    local defaultStoreConfig = thanosConfig.commonConfig {
      namespace: namespace,
      logLevel: 'debug',

      indexCache+: (
        if !std.objectHas(envConfig, 'indexMemcached') then {
          type: 'IN-MEMORY',
          config+: {
            max_size: '1GB',
          },
        }
        else {
          type: 'memcached',
          config+: {
            addresses: ['memcached-thanos-index-cache.%s.svc.cluster.local:11211' % namespace],
            max_item_size: '16MB',
          },
        }
      ),

      // TODO tune chunk pool size. In chef, this was set to 10% of node
      // memory: 1.2GB in most environments.

      // This is not an officially-supported flag. If
      // https://github.com/thanos-io/kube-thanos/issues/175 is resolved, we
      // can remove the patch below.
      maxTime: '-24h',
    } + (
      if !std.objectHas(envConfig, 'bucketMemcached') then
        {}
      else {
        bucketCache+: {
          type: 'memcached',
          config+: {
            addresses: ['memcached-thanos-bucket-cache.%s.svc.cluster.local:11211' % namespace],
            max_item_size: '16MB',
          },
        },
      }
    ) + self.withDisk(envConfig.diskSize, envConfig.diskStorageClass);
    local mergedConfig = defaultStoreConfig + envConfig.config;

    local bucketSecret = self.objectStoreConfig(envConfig.bucketName);
    local serviceAccount = self.serviceAccount(env, clusters.clusters[env].googleProject);

    local store = thanos.storeShards(mergedConfig) + {
      shards+: {
        ['shard%s' % i]: super['shard' + i] {
          statefulSet+: {
            spec+: {
              template+: {
                spec+: {
                  serviceAccountName: 'prometheus',
                  containers: common.patchElementByName({
                    args+: [
                      '--max-time=%s' % mergedConfig.maxTime,
                      '--store.grpc.series-sample-limit=100000000',
                    ],
                  }, 'thanos-store', super.containers),
                } + (
                  if std.objectHas(envConfig, 'nodePool') then
                    common.withNodePoolAffinity(envConfig.nodePool)
                  else
                    {}
                ),
              },
            },
          } + common.withDeploymentPodLabel(thanosConfig.type, 'thanos-store'),

          service+: {
            metadata+: {
              annotations+: {
                'external-dns.alpha.kubernetes.io/hostname':
                  common.externalURL(
                    'thanos-store-internal-%d' % i,
                    env,
                    fqdn=true,
                  ),
              },
            },
          },
        }
        for i in std.range(0, mergedConfig.shards - 1)
      },

      serviceMonitor+: thanosConfig.withStandardMetricLabels(shard=null),
    };

    {
      store: store,
      objectStoreConfig: bucketSecret,
      serviceAccount: serviceAccount,
    } + (
      if !std.objectHas(envConfig, 'indexMemcached') then
        {}
      else
        {
          indexMemcached:
            memcached.new(
              name='thanos-index-cache',
              namespace=namespace,
              type=thanosConfig.type,
              nodePool=(if std.objectHas(envConfig, 'nodePool') then envConfig.nodePool else null),
              chartValues=envConfig.indexMemcached.values,
              metricRelabelings=thanosConfig.standardMetricLabels(),
            ),
        }
    ) + (
      if !std.objectHas(envConfig, 'bucketMemcached') then
        {}
      else
        {
          bucketMemcached:
            memcached.new(
              name='thanos-bucket-cache',
              namespace=namespace,
              type=thanosConfig.type,
              nodePool=(if std.objectHas(envConfig, 'nodePool') then envConfig.nodePool else null),
              chartValues=envConfig.bucketMemcached.values,
              metricRelabelings=thanosConfig.standardMetricLabels(),
            ),
        }
    ),

  // storageClass=null uses the default
  withDisk(size, storageClass):: {
    volumeClaimTemplate+: {
      spec: {
        accessModes: ['ReadWriteOnce'],
        storageClassName: storageClass,
        resources: {
          requests: {
            storage: size,
          },
        },
      },
    },
  },

  // Don't panic! This Secret is not a secret.  The thanos-operator expects to
  // load object store config for the store from a Secret, which can indeed
  // contain private keys. We don't make use of this, instead using GKE
  // workload identity for keyless bucket access.  We still need to create
  // this "Secret" to store the bucket name though.
  objectStoreConfig(bucketName):: {
    apiVersion: 'v1',
    kind: 'Secret',
    metadata: {
      name: 'thanos-objstore',
    },
    stringData: {
      'objstore-gcs.yaml': |||
        ---
        type: GCS
        config:
          bucket: %s
      ||| % bucketName,
    },
  },

  serviceAccount(env, googleProject)::
    k.core.v1.serviceAccount.new(name='prometheus') {
      metadata+: {
        annotations+: {
          'iam.gke.io/gcp-service-account':
            '%s-prometheus-sa@%s.iam.gserviceaccount.com' % [
              env,
              googleProject,
            ],
        },
      },
    },
}

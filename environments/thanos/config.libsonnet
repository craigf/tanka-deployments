local common = import 'common.libsonnet';

{
  type: 'monitoring',

  commonConfig: {
    version: 'v0.18.0',
    image: 'quay.io/thanos/thanos:' + self.version,
    logFormat: 'json',
    serviceMonitor: true,
    replicas: 1,
    objectStorageConfig: {
      name: 'thanos-objstore',
      key: 'objstore-gcs.yaml',
    },
  },

  // To be mixed into a ServiceMonitor
  withStandardMetricLabels(shard='default'):: {
    spec+: {
      endpoints: [
        endpoint {
          relabelings+: $.standardMetricLabels(shard=shard),
        }
        for endpoint in super.endpoints
      ],
    },
  },

  standardMetricLabels(type='monitoring', stage='main', shard='default')::
    common.standardMetricLabels(type, stage, shard),
}

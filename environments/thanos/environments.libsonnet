local clusters = import 'clusters.libsonnet';
local common = import 'common.libsonnet';

local defaultStoreShardResources = {
  requests: {
    cpu: '1',
    memory: '5Gi',
  },
  limits: {
    memory: '5Gi',
  },
};

local defaultStoreShardDiskSize = '5Gi';
local defaultStorageClass = 'ssd';

local storeMemcached(indexMemory='2Gi',
                     indexReplicas=1,
                     bucketMemory='2Gi',
                     bucketReplicas=1) = {
  indexMemcached: {
    values: {
      resources: {
        requests: {
          memory: indexMemory,
        },
      },
      replicaCount: indexReplicas,
    },
  },
  bucketMemcached: {
    values: {
      resources: {
        requests: {
          memory: bucketMemory,
        },
      },
      replicaCount: bucketReplicas,
    },
  },
};

{
  gprd: {
    store: {
      bucketName: 'gitlab-gprd-prometheus',
      nodePool: 'default',
      config: {
        shards: 10,
        replicas: 2,
        resources: defaultStoreShardResources,
      },
      diskSize: '30Gi',
      diskStorageClass: defaultStorageClass,
    } + storeMemcached(indexReplicas=5, bucketReplicas=5),
  },

  gstg: {
    store: {
      bucketName: 'gitlab-gstg-prometheus',
      nodePool: 'default',
      config: {
        resources: defaultStoreShardResources,
        shards: 3,
        replicas: 2,
      },
      diskSize: '30Gi',
      diskStorageClass: defaultStorageClass,
    } + storeMemcached(),
  },

  ops: {
    store: {
      bucketName: 'gitlab-ops-prometheus',
      config: {
        shards: 1,
        replicas: 2,
        resources: defaultStoreShardResources,
      },
      diskSize: defaultStoreShardDiskSize,
      diskStorageClass: defaultStorageClass,
    } + storeMemcached(indexReplicas=3, bucketReplicas=3),

    query: {
      config: {
        replicas: 3,
        resources: {
          requests: {
            cpu: '2',
            memory: '8Gi',
          },
          limits: {
            memory: '8Gi',
          },
        },

        // We can use dns (via external-dns) to discover GKE-deployed prometheus
        // and thanos-store pods.  For GCE-deployed endpoints, we have a
        // problem: we can't use `.internal` URLs outside of the GCP project the
        // URL refers to. We could use static IPs, or create DNS entries for the
        // private IPs of these endpoints.
        stores: [
          'dns+%s:10901' % common.externalURL('thanos-store-internal-%d' % i, env)
          for env in clusters.allRegional()
          for i in std.range(0, $[env].store.config.shards - 1)
        ] + [
          // Prometheus resources are currently managed by helm, outside of this
          // repo. When we move this to tanka, we might want to move this part
          // of the function to encapsulate prometheus concerns in one place.
          'dns+%s:10901' % common.externalURL('prometheus-internal', env)
          for env in clusters.all()
        ] + [
          // GCE prometheus private IPs.
          // This is not the most robust solution, and we should replace it with
          // DNS service discovery, but we're likely to move these jobs to
          // kube-deployed prometheus before the effort paid into DNS discovery
          // would be paid off.
          //
          // knife search 'recipes:gitlab-prometheus\:\:thanos AND cloud_provider:gce AND (thanos-rule_enable:true OR thanos-sidecar_enable:true)' -F json -a hostname -a ipaddress | jq -r ".rows[] | to_entries[] | \"  '\" + .value.ipaddress + \":10901', // \" + .key" | sort -u
          '10.219.1.10:10901',  // prometheus-01-inf-gprd.c.gitlab-production.internal
          '10.219.1.11:10901',  // prometheus-db-02-inf-gprd.c.gitlab-production.internal
          '10.219.1.4:10901',  // prometheus-app-01-inf-gprd.c.gitlab-production.internal
          '10.219.1.5:10901',  // prometheus-app-02-inf-gprd.c.gitlab-production.internal
          '10.219.1.8:10901',  // prometheus-db-01-inf-gprd.c.gitlab-production.internal
          '10.219.1.9:10901',  // prometheus-02-inf-gprd.c.gitlab-production.internal
          '10.226.1.11:10901',  // prometheus-02-inf-gstg.c.gitlab-staging-1.internal
          '10.226.1.12:10901',  // prometheus-app-02-inf-gstg.c.gitlab-staging-1.internal
          '10.226.1.14:10901',  // prometheus-db-02-inf-gstg.c.gitlab-staging-1.internal
          '10.226.1.19:10901',  // prometheus-test-01-inf-gstg.c.gitlab-staging-1.internal
          '10.226.1.3:10901',  // prometheus-db-01-inf-gstg.c.gitlab-staging-1.internal
          '10.226.1.8:10901',  // prometheus-01-inf-gstg.c.gitlab-staging-1.internal
          '10.226.1.9:10901',  // prometheus-app-01-inf-gstg.c.gitlab-staging-1.internal
          '10.232.3.15:10901',  // prometheus-app-01-inf-pre.c.gitlab-pre.internal
          '10.232.3.17:10901',  // prometheus-01-inf-pre.c.gitlab-pre.internal
          '10.240.3.3:10901',  // prometheus-01-inf-testbed.c.gitlab-testbed.internal
          '10.250.25.101:10901',  // thanos-rule-01-inf-ops.c.gitlab-ops.internal
          '10.250.25.102:10901',  // thanos-rule-02-inf-ops.c.gitlab-ops.internal
          '10.250.8.16:10901',  // prometheus-02-inf-ops.c.gitlab-ops.internal
          '10.250.8.4:10901',  // prometheus-01-inf-ops.c.gitlab-ops.internal
        ],
      },
    },

    queryFrontend: {
      config: {
        logQueriesLongerThan: '30s',
        replicas: 3,
        resources: {
          requests: {
            cpu: '2',
            memory: '2Gi',
          },
          limits: {
            memory: '2Gi',
          },
        },
      },

      queryRangeMemcached: {
        values: {
          replicaCount: 10,
          resources: {
            requests: {
              memory: '1Gi',
            },
          },
        },
      },

      labelMemcached: {
        values: {
          replicaCount: 10,
          resources: {
            requests: {
              memory: '1Gi',
            },
          },
        },
      },

      ingress: {
        ipName: 'thanos-query-frontend-ops',
        domain: 'thanos.gitlab.net',
      },
    },
  },

  pre: {
    store: {
      bucketName: 'gitlab-pre-prometheus',
      config: {
        resources: defaultStoreShardResources,
        shards: 1,
        replicas: 1,
      },
      diskSize: defaultStoreShardDiskSize,
      diskStorageClass: defaultStorageClass,
    } + storeMemcached(),
  },

  'org-ci': {
    store: {
      bucketName: 'gitlab-org-ci-prometheus',
      config: {
        resources: defaultStoreShardResources,
        shards: 1,
        replicas: 2,
      },
      diskSize: defaultStoreShardDiskSize,
      diskStorageClass: defaultStorageClass,
    } + storeMemcached(),
  },

  // Dev environment, refers to a local development cluster.
  dev: {
    store: {
      bucketName: 'dummy',
      config: {
        logFormat: 'logfmt',
        resources: {},
        replicas: 1,
        shards: 1,
      },
      diskSize: '1Gi',
      diskStorageClass: null,

      indexMemcached: {
        values: {
          extraEnv+: {
            MEMCACHED_EXTRA_FLAGS: '--max-item-size=1M',
          },
          resources: {
            requests: {
              memory: '64Mi',
            },
          },
          replicaCount: 1,
        },
      },

      bucketMemcached: {
        values: {
          extraEnv+: {
            MEMCACHED_EXTRA_FLAGS: '--max-item-size=1M',
          },
          resources: {
            requests: {
              memory: '64Mi',
            },
          },
          replicaCount: 1,
        },
      },
    },

    query: {
      config: {
        logFormat: 'logfmt',
        replicas: 1,
        resources: {},
      },
    },

    queryFrontend: {
      config: {
        logFormat: 'logfmt',
        replicas: 1,
        resources: {},
      },

      queryRangeMemcached: {
        values: {
          extraEnv+: {
            MEMCACHED_EXTRA_FLAGS: '--max-item-size=1M',
          },
          replicaCount: 1,
          resources: {
            requests: {
              memory: '64Mi',
            },
          },
        },
      },

      labelMemcached: {
        values: {
          extraEnv+: {
            MEMCACHED_EXTRA_FLAGS: '--max-item-size=1M',
          },
          replicaCount: 1,
          resources: {
            requests: {
              memory: '64Mi',
            },
          },
        },
      },
    },
  },
}

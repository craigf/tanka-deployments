local common = import 'common.libsonnet';
local thanosConfig = import 'config.libsonnet';
local environments = import 'environments.libsonnet';
local k = import 'ksonnet-util/kausal.libsonnet';
local thanos = import 'kube-thanos/thanos.libsonnet';
local memcached = import 'memcached/memcached.libsonnet';

local service = k.core.v1.service;

{
  new(env, namespace)::
    local envConfig = environments[env];
    local defaultQueryConfig = thanosConfig.commonConfig {
      namespace: namespace,
      replicaLabels: ['replica', 'prometheus_replica'],
    };
    local query = thanos.query(defaultQueryConfig + envConfig.query.config) + {
      deployment+: common.withDeploymentPodLabel(thanosConfig.type),
      serviceMonitor+: thanosConfig.withStandardMetricLabels(),
    };

    // TODO this is a new component, and this config is completely untuned.
    local defaultQueryFrontendConfig = thanosConfig.commonConfig {
      namespace: namespace,
      downstreamURL: 'http://%s.%s.svc.cluster.local.:%d' % [
        query.service.metadata.name,
        query.service.metadata.namespace,
        query.service.spec.ports[1].port,
      ],
    } + (
      if !std.objectHas(envConfig.queryFrontend, 'labelMemcached') then
        {}
      else {
        labelsCache+: {
          type: 'memcached',
          config+: {
            addresses: ['memcached-thanos-qfe-labels.%s.svc.cluster.local:11211' % namespace],
            max_item_size: '16MB',
          },
        },
      }
    ) + (
      if !std.objectHas(envConfig.queryFrontend, 'queryRangeMemcached') then
        {}
      else {
        queryRangeCache+: {
          type: 'memcached',
          config+: {
            addresses: ['memcached-thanos-qfe-query-range.%s.svc.cluster.local:11211' % namespace],
            max_item_size: '16MB',
          },
        },
      }
    );

    {
      query: query {
        // ILB for intra-VPC access, e.g. from thanos-rule. Once we've migrated all
        // components to kubernetes, we can use the service clusterIP instead
        // and remove this.
        // thanos-rule needs to retrieve non-stale data from thanos-query,
        // uncached, so cannot use the frontend.
        serviceInternal: self.service {
          metadata+: {
            name+: '-ilb',
            labels+: {
              'app.kubernetes.io/instance': super['app.kubernetes.io/instance'] + '-ilb',
            },
            annotations+: {
              'networking.gke.io/load-balancer-type': 'Internal',
              'external-dns.alpha.kubernetes.io/hostname':
                common.externalURL('thanos-query-internal', env, fqdn=true),
            },
          },
          spec+: {
            type: 'LoadBalancer',
          },
        },
      },

      queryFrontend: thanos.queryFrontend(defaultQueryFrontendConfig + envConfig.queryFrontend.config) + {
        deployment+: common.withDeploymentPodLabel(thanosConfig.type),
        serviceMonitor+: thanosConfig.withStandardMetricLabels(),

        // In order to use "classic" GKE HTTPS load balancing (not
        // "container-native", which we don't use anywhere yet at the time of
        // writing), we need a service with type=NodePort.
        service+: service.mixin.spec.withType('NodePort'),

        // ILB for intra-VPC access, e.g. from grafana. Once we've migrated all
        // components to kubernetes, we can use the service clusterIP instead
        // and remove this.
        serviceInternal: self.service {
          metadata+: {
            name+: '-ilb',
            labels+: {
              'app.kubernetes.io/instance': super['app.kubernetes.io/instance'] + '-ilb',
            },
            annotations+: {
              'networking.gke.io/load-balancer-type': 'Internal',
              'external-dns.alpha.kubernetes.io/hostname':
                common.externalURL('thanos-query-frontend-internal', env, fqdn=true),
            },
          },
          spec+: {
            type: 'LoadBalancer',
          },
        },
      } + (
        if !std.objectHas(envConfig.queryFrontend, 'labelMemcached') then
          {}
        else {
          labelMemcached: memcached.new(
            name='thanos-qfe-labels',
            namespace=namespace,
            type=thanosConfig.type,
            chartValues=envConfig.queryFrontend.labelMemcached.values,
            metricRelabelings=thanosConfig.standardMetricLabels(),
          ),
        }
      ) + (
        if !std.objectHas(envConfig.queryFrontend, 'queryRangeMemcached') then
          {}
        else {
          queryRangeMemcached: memcached.new(
            name='thanos-qfe-query-range',
            namespace=namespace,
            type=thanosConfig.type,
            chartValues=envConfig.queryFrontend.queryRangeMemcached.values,
            metricRelabelings=thanosConfig.standardMetricLabels(),
          ),
        }
      ),

    },

  withIngress(ipName, domain)::
    {
      local qfe = self.queryFrontend,

      queryFrontend+: {
        service+: {
          metadata+: {
            annotations+: {
              'cloud.google.com/backend-config': std.manifestJsonEx({ default: 'thanos-query-frontend' }, '  '),
            },
          },
        },
      },

      ingress: {
        apiVersion: 'extensions/v1beta1',
        kind: 'Ingress',
        metadata: {
          name: 'thanos-query-frontend',
          annotations: {
            'kubernetes.io/ingress.global-static-ip-name': ipName,
            'networking.gke.io/managed-certificates': 'thanos-query-frontend',
          },
        },
        spec: {
          backend: {
            serviceName: qfe.service.metadata.name,
            servicePort: 'http',
          },
        },
      },

      // This secret, thanos-iap-oauth, must already exist. See
      // environments/thanos/README.md.
      backendConfig: common.backendConfig('thanos-query-frontend', 'thanos-iap-oauth'),
      managedCert: common.managedCert('thanos-query-frontend', domain),
    },
}

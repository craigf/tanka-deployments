# Woodhouse

https://gitlab.com/gitlab-com/gl-infra/woodhouse

## Deployment

Woodhouse can take either a flag or an environment variable for each of its
configuration elements. See [Woodhouse's
readme](https://gitlab.com/gitlab-com/gl-infra/woodhouse) for information on
that config.

We currently manage secrets outside of tanka, by creating them directly.

Bootstrap the secrets:

```
kubectl create namespace woodhouse
kubectl --namespace woodhouse create secret generic woodhouse
kubectl --namespace woodhouse edit secret woodhouse
```

Now write in the secret environment values for Woodhouse under `data`. The
values must be base64 encoded.

There is a kubectl plugin that streamlines secret modification with regard to
base 64: https://github.com/rajatjindal/kubectl-modify-secret, or you can
convert the values to base64 in any way you choose.

Note that the namespace will have labels added to it by the first tanka run, but
it needs to exist in order to add the secret.

Woodhouse is slightly different from most of our deployments, in that it depends
on an external variable - the image tag. In CI, this is defaulted to latest, and
overridden by API-triggered pipelines from Woodhouse's own pipelines, but in dev
you'll need to set it:

```
tk diff --with-prune environments/woodhouse --name woodhouse/dev --tla-str tag=latest
```

{
  ops: {
    config: {
      GITLAB_INCIDENT_ISSUE_PROJECT_PATH: 'gitlab-com/gl-infra/production',
      INCIDENT_SLACK_CHANNEL: 'CB7P5CJS1',  // incident-management
      SHUTDOWN_SLEEP_SECONDS: '10',
      MAX_REQUESTS_PER_SECOND_PER_IP: '20',
      INCIDENT_CHANNEL_NAME_PREFIX: 'incident',
      PAGERDUTY_ESCALATION_POLICY_EOC: 'P7IG7DS',
      PAGERDUTY_ESCALATION_POLICY_IMOC: 'P3N1TU2',
    },
    replicas: 2,
    resources: {
      requests: {
        cpu: '1',
        memory: '256Mi',
      },
      limits: {
        memory: '256Mi',
      },
    },
    ingress: {
      domain: 'woodhouse.ops.gitlab.net',
      staticIPName: 'woodhouse-ops',
    },
  },

  gstg: {
    config: {
      GITLAB_INCIDENT_ISSUE_PROJECT_PATH: 'gitlab-com/gl-infra/woodhouse-integration-test',
      INCIDENT_SLACK_CHANNEL: 'C01CB7PNR0R',  // woodhouse-staging
      GLOBAL_SLASH_COMMAND: 'woodhouse-staging',
    },
    replicas: 1,
    resources: {
      requests: {
        cpu: '250m',
        memory: '64Mi',
      },
      limits: {
        memory: '64Mi',
      },
    },
    nodePool: 'default',
    ingress: {
      domain: 'woodhouse.gstg.gitlab.net',
      staticIPName: 'woodhouse-gstg',
    },
  },

  dev: {
    config: {
      GITLAB_INCIDENT_ISSUE_PROJECT_PATH: 'dummy',
      INCIDENT_SLACK_CHANNEL: 'dummy',
      LOG_FORMAT: 'logfmt',
    },
    replicas: 1,
    resources: {},
  },
}

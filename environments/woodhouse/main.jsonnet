local clusters = import 'clusters.libsonnet';
local environments = import 'environments.libsonnet';
local woodhouse = import 'woodhouse.libsonnet';

local namespace = 'woodhouse';

function(tag='latest') {
  [env]: clusters.environment(env, namespace, 'woodhouse') + {
    local envConfig = environments[env],
    data: {
      woodhouse: woodhouse.new(env, namespace, tag),
    } + (
      if std.objectHas(envConfig, 'ingress') then
        woodhouse.ingress(env)
      else {}
    ),
  }
  for env in std.objectFields(environments)
}

local common = import 'common.libsonnet';
local environments = import 'environments.libsonnet';
local k = import 'ksonnet-util/kausal.libsonnet';

local container = k.core.v1.container;
local port = k.core.v1.containerPort;

local probe = {
  httpGet: {
    path: '/ready',
    port: 'http',
  },
};

{
  new(env, namespace, tag):: {
    namespace: common.namespace(namespace),
    deployment: $.deployment(env, tag),
    service: $.serviceFor(self.deployment),
    serviceMonitor: $.serviceMonitor(env),
  },

  deployment(env, tag)::
    local envConfig = environments[env];
    local config = {
      LOG_FORMAT: 'json',
    } + envConfig.config;

    common.deployment(
      name='woodhouse',
      type='woodhouse',
      replicas=envConfig.replicas,
      containers=[
        container.new(name='woodhouse', image='registry.gitlab.com/gitlab-com/gl-infra/woodhouse:%s' % tag)
        + container.withPorts([port.new('http', 8080), port.new('metrics', 8081)])
        + container.withEnvMap(config)
        + {
          // Take imagePullPolicy private to remove it from the final object.
          // We want to rely on the default behaviour of imagePullPolicy to
          // avoid using stale `:latest` images.
          imagePullPolicy:: super.imagePullPolicy,

          command: ['woodhouse', 'serve'],
          envFrom: [
            {
              secretRef: { name: 'woodhouse' },
            },
          ],
          livenessProbe: probe,
          readinessProbe: probe,
          resources: envConfig.resources,
        },
      ],
    ) + {
      metadata+: {
        labels+: {
          name: 'woodhouse',
        },
      },
      spec+: {
        template+: {
          spec+: (
            if std.objectHas(envConfig, 'nodePool') then
              common.withNodePoolAffinity(envConfig.nodePool)
            else {}
          ),
        },
      },
    },

  serviceFor(deployment)::
    k.util.serviceFor(deployment) + {
      spec+: {
        type: 'NodePort',
      },
    },

  serviceMonitor(env):: {
    apiVersion: 'monitoring.coreos.com/v1',
    kind: 'ServiceMonitor',
    metadata: {

      name: 'woodhouse',
    },
    spec: {
      endpoints: [
        {
          interval: '15s',
          path: '/metrics',
          port: 'woodhouse-metrics',
          relabelings: [
            {
              targetLabel: 'type',
              replacement: 'woodhouse',
            },
            {
              targetLabel: 'tier',
              replacement: 'sv',
            },
            {
              targetLabel: 'stage',
              replacement: 'main',
            },
            {
              targetLabel: 'shard',
              replacement: 'default',
            },
            {
              targetLabel: 'environment',
              replacement: env,
            },
          ],
        },
      ],
      selector: {
        matchLabels: {
          name: 'woodhouse',
        },
      },
    },
  },

  ingress(env)::
    local envConfig = environments[env];
    {
      ingress: {
        apiVersion: 'extensions/v1beta1',
        kind: 'Ingress',
        metadata: {
          name: 'woodhouse',
          annotations: {
            'kubernetes.io/ingress.global-static-ip-name': envConfig.ingress.staticIPName,
            'networking.gke.io/managed-certificates': 'woodhouse',
          },
        },
        spec: {
          rules: [
            {
              host: envConfig.ingress.domain,
              http: {
                paths: [
                  {
                    backend: {
                      serviceName: 'woodhouse',
                      servicePort: 'woodhouse-http',
                    },
                    path: '/*',
                  },
                ],
              },
            },
          ],
        },
      },
      managedCert: common.managedCert('woodhouse', envConfig.ingress.domain),
    },
}

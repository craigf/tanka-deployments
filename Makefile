.PHONY: default
default: generate

.PHONY: generate
generate: vendor generate-ci-config

.PHONY: vendor
vendor:
	jb install
	tk tool charts vendor

.PHONY: generate-ci-config
generate-ci-config:
	jsonnet --string .gitlab-ci.jsonnet > .gitlab-ci.yml
